<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{
    UserController,
    CategoryController,
    API\AuthController,
    API\FormController
};

Route::group(['middleware' => 'auth:sanctum'], function(){
    Route::post('article/create', [FormController::class, 'create']);
    Route::get('article/edit/{id}', [FormController::class, 'edit']);
    Route::post('article/edit/{id}', [FormController::class, 'update']);
    Route::get('article/delete/{id}', [FormController::class, 'delete']);

    Route::post('category/create', [CategoryController::class, 'create']);
    Route::get('category/edit/{id}', [CategoryController::class, 'edit']);
    Route::post('category/edit/{id}', [CategoryController::class, 'update']);
    Route::get('category/delete/{id}', [CategoryController::class, 'delete']);

    Route::post('user/create', [UserController::class, 'create']);
    Route::get('user/edit/{id}', [UserController::class, 'edit']);
    Route::post('user/edit/{id}', [UserController::class, 'update']);
    Route::get('user/delete/{id}', [UserController::class, 'delete']);
    
    Route::get('logout', [AuthController::class, 'logout']);
});

Route::get('users', [UserController::class, 'users']);

Route::post('login', [AuthController::class, 'login']);