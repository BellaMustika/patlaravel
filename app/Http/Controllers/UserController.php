<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function users(User $user)
    {
        $users = $user->all();

        return response()->json($users);
    }

    public function create(Request $request)
    {
        
        $request->validate([
            'email' => 'required',
            'username' => 'required',
            'role_id' => 'required',
            'password' => 'required'
        ]);

        $user = new User;
        $user->email = $request->email;
        $user->username = $request->username;
        $user->role_id = $request->role_id;
        $user->password = bcrypt($request->password);
        
        // dd($user);
        $user->save();

        return response()->json([
            'message' => 'Berhasil membuat user',
            'data_user' => $user
        ], 200);
    }

    public function edit($id)
    {
        $user = User::find($id);
        return response()->json([
            'message' => 'success',
            'data_user' => $user
        ], 200);
    }

    public function update(Request $request, $id)
    {
        $user = user::find($id);
        $request->validate([
            'email' => 'required',
            'username' => 'required',
            'role_id' => 'required',
            'password' => 'required'
        ]);

        $user->update([
            'email' => $request->email,
            'username' => $request->username,
            'role_id' => $request->role_id,
            'password' => $request->password
        ]);
         
        return response()->json([
            'message' => 'Berhasil mengubah user',
            'data_article' => $user
        ], 200);

    }

    public function delete($id)
    {
        $user = User::find($id)->delete();
        return response()->json([
            'message' => 'Berhasil menghapus user'
        ], 200);
    }


}