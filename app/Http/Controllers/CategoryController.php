<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    public function create(Request $request)
    {

        $request->validate([
            'name' => 'required',
        ]);

        $category = new Category;
        $category->name = $request->name;
        
        // dd($category->all());
        $category->save();

        return response()->json([
            'message' => 'Berhasil membuat category',
            'data_category' => $category
        ], 200);
    }

    public function edit($id)
    {
        $category = Category::find($id);
        return response()->json([
            'message' => 'success',
            'data_category' => $category
        ], 200);
    }

    public function update(Request $request, $id)
    {
        $category = Category::find($id);
        $request->validate([
            'name' => 'required'
        ]);

        $category->update([
            'name' => $request->name
        ]);
         
        return response()->json([
            'message' => 'Berhasil mengedit category',
            'data_category' => $category
        ], 200);

    }

    public function delete($id)
    {
        $category = Category::find($id)->delete();
        return response()->json([
            'message' => 'Berhasil menghapus kategori'
        ], 200);
    }
}